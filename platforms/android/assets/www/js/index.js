
var db;
var detCom;

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },
    
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id); 
    }
};

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {

    console.log('onDeviceReady');

    $(".addArticles").hide();

    // 0 = no definido
    // 1 = remito
    // 2 = factura
    window.localStorage.setItem('comprobante',0);

    //OBTIENE LA FECHA DE ULTIMO LOGIN
    var FecUltActL =  window.localStorage.getItem("FecUltActL");
    if(FecUltActL == null){
        $(".FecUltActL").html('Nunca'); 
    }else{
        $(".FecUltActL").html(FecUltActL);
    }

    //OBTIENE LA FECHA DE ULTIMA VEZ DE SYNC CON EMPRESAS
    var FecUltActE =  window.localStorage.getItem("FecUltActE");
    if(FecUltActE == null){
        $(".FecUltActE").html('Nunca'); 
    }else{
        $(".FecUltActE").html(FecUltActE);
    }

    //OBTIENE LA FECHA DE ULTIMA VEZ DE SYNC CON PRECIOS
    var FecUltActM =  window.localStorage.getItem("FecUltActM");
    if(FecUltActM == null){
        $(".FecUltActM").html('Nunca'); 
    }else{
        $(".FecUltActM").html(FecUltActM);
    }    

    //OBTIENE LA FECHA DE ULTIMA VEZ DE SYNC CON PRECIOS
    var FecUltActM =  window.localStorage.getItem("FecUltActM");
    if(FecUltActM == null){
        $(".FecUltActM").html('Nunca'); 
    }else{
        $(".FecUltActM").html(FecUltActM);
    }

    //OBTIENE LA FECHA DE ULTIMA VEZ DE SYNC DE REMITOS
    var FecUltActR =  window.localStorage.getItem("FecUltActR");
    if(FecUltActR == null){
        $(".FecUltActR").html('Nunca'); 
    }else{
        $(".FecUltActR").html(FecUltActR);
    }

    //OBTIENE LA FECHA DE ULTIMA VEZ DE SYNC DE FACTURA
    var FecUltActF =  window.localStorage.getItem("FecUltActF");
    if(FecUltActF == null){
        $(".FecUltActF").html('Nunca'); 
    }else{
        $(".FecUltActF").html(FecUltActF);
    }
    
    //OBTIENE LA FECHA DE ULTIMA VEZ DE SYNC DE GESTION DE REPARTO
    var FecUltActG =  window.localStorage.getItem("FecUltActG");
    if(FecUltActG == null){
        $(".FecUltActG").html('Nunca'); 
    }else{
        $(".FecUltActG").html(FecUltActG);
    }     

    //RECUERDO LOS DATOS DE CONEXIÓN.
    var configHost = window.localStorage.getItem("webservice");
    if(configHost != null){
        $("#inputWS").val(configHost);
    }

    var configBd = window.localStorage.getItem("basededatos");
    if(configBd != null){
        $("#inputBD").val(configBd);
    }

    var configHUs = window.localStorage.getItem("user");
    if(configHUs != null){
        $("#inputUser").val(configHUs);
        $(".nameUser").html(configHUs);
    }

    var configPass = window.localStorage.getItem("pass");
    if(configPass != null){
        $("#inputPass").val(configPass);
    }

    //VERIFICO SI YA HABILITÓ EL MODO ADMINISTRADOR.
    var isAdmin = window.localStorage.getItem("isAdmin");

    if(isAdmin != 1){
        $("#SendAllDataToItris").hide();
        $("#resetApp").hide();
    }else{
        $("#SendAllDataToItris").show();
        $("#resetApp").show();
    }


    //SETEO INICIAL 
    var status = window.localStorage.getItem("statusV6");
    
    /*Activa el modo debug*/
    //var status = window.localStorage.getItem("statusV1B");
    
    console.log('Este es el status' + status);
    
    window.localStorage.setItem("ItsSendMail","false");

    if(status==null){

        db = window.openDatabase("TOSTANAPP", "1.0", "Tostanapp", 200000);
        db.transaction(
            function populateDB(tx) {

                //Gestión de reparto.
                tx.executeSql('DROP TABLE IF EXISTS EST_CLI_APP');
                tx.executeSql('CREATE TABLE IF NOT EXISTS EST_CLI_APP (id INTEGER PRIMARY KEY AUTOINCREMENT, FECHA, CLIENTE, RAZ_SOCIAL, MOTIVO, USUARIO, OBSERVACIONES, GESTIONADO, CENTRALIZADO)');

                //Esto lo carga la app para el alta de cliente.
                tx.executeSql('DROP TABLE IF EXISTS ERP_CLIENTES_MAIL');
                tx.executeSql('CREATE TABLE IF NOT EXISTS ERP_CLIENTES_MAIL (id INTEGER PRIMARY KEY AUTOINCREMENT, raz_social, tel, tipDoc, catIva, numDoc, Detalle, centralizado)');
                
                //Esto lo alimenta Itris cuando sincronizás.
                tx.executeSql('DROP TABLE IF EXISTS PRE_VEN_APP');
                tx.executeSql('CREATE TABLE IF NOT EXISTS PRE_VEN_APP (ID unique, DESCRIPCION, ID_EMPRESA, ID_VENDEDOR, VENDEDOR, PRE, LISTA, FK_ERP_ARTICULOS, DES_ART, PRECIO, _FK_ITRIS_USERS)');
                //tx.executeSql('INSERT INTO PRE_VEN_APP (ID, DESCRIPCION, ID_EMPRESA, ID_VENDEDOR, VENDEDOR, PRE, LISTA, FK_ERP_ARTICULOS, DES_ART, PRECIO, _FK_ITRIS_USERS) VALUES ("id","DESCRIPCION", "ID_EMPRESA", "ID_VENDEDOR", "VENDEDOR", "PRE", "LISTA", "FK_ERP_ARTICULOS", "DES_ART", "PRECIO", "_FK_ITRIS_USERS")');
                
                //Pedidos para ITRIS
                tx.executeSql('DROP TABLE IF EXISTS APP_PEDIDOS');
                tx.executeSql('CREATE TABLE IF NOT EXISTS APP_PEDIDOS (ID INTEGER PRIMARY KEY AUTOINCREMENT, FECHA, DATA, CENTRALIZADO)');

            },function errorCB(err) {

                navigator.notification.alert(
                    "ATENCIÓN EXISTIÓ UN ERROR GRAVE EN LA CONSTRUCCIÓN DE LA APP, POR FAVOR CONTACTESE CON (lcondori@gmail.com) " + err.message,
                    function msje2() {

                    },
                    'INFORMACIÓN',
                    'Cerrar'
                );                

            },function successCB() {

                navigator.notification.alert(
                    "¡Construímos las bases de datos de manera correcta!",
                    function msje1() {

                    },
                    'INFORMACIÓN',
                    'Aceptar'
                );                

            }
        );

        window.localStorage.setItem("statusV6",1);

    }

    window.localStorage.setItem("modoDev",0);

    //MODO DESARROLLADOR
    $("#modoAdmin").click(function(){

        if(window.localStorage.getItem("isAdmin")==1){
            return;
        }

        var modoDev = window.localStorage.getItem("modoDev");
        modoDev++;
        window.localStorage.setItem("modoDev",modoDev);

        if(modoDev==9){
            function onPrompt(results) {
                //alert("You selected button number " + results.buttonIndex + " and entered " + results.input1);
                if(results.input1=="oidmortales"){

                    navigator.notification.alert(
                        "¡Felicitaciones! ahora sos administrador. Esto significa que vas a poder forzar la centralización y poder resetear la app. Esto se verá reflejado en el menú principal.",
                        function ms1() {
                            window.localStorage.setItem("isAdmin",1);
                            location.reload();
                        },
                        'INFORMACIÓN',
                        'Cerrar'
                    );

                }else{

                    navigator.notification.alert(
                        "La palabra clave ingresada ["+results.input1+"] no es correcta, consulte con su proveedor.",
                        function msjess() {
                            window.localStorage.setItem("modoDev",0);
                        },
                        'ERROR',
                        'Cerrar'
                    );

                }
            }
            
            navigator.notification.prompt(
                '¡Atención! estás a punto de convertirte en administrador de esta aplicación, por favor ingresá la palabra clave provista por el proveedor.',  // message
                onPrompt,                  // callback to invoke
                'Modo Desarrollador',            // title
                ['Ok','Exit'],             // buttonLabels
                ''                 // defaultText
            );

        }

    })

}

function reseatApp(){

    function onConfirm(buttonIndex) {
        //alert('You selected button ' + buttonIndex);

        if(buttonIndex==1){
            localStorage.clear();

            navigator.notification.alert(
                "¡Restauración de la app fue realizado con éxito!",
                function msw1() {
                    location.reload();
                },
                'INFORMACIÓN',
                'Cerrar'
            );

        }

    }
    
    navigator.notification.confirm(
        'Estás a punto de reestablecer todos los ajustes por default. Esto borrará la base de datos, y toda información guardada en la APP. Esta información no se puede recuperar. Vas a borrar toda la información.', // message
         onConfirm,            // callback to invoke with index of button pressed
        'ATENCIÓN',           // title
        ['Restaurar','Salir']     // buttonLabels
    );

    

}


app.initialize();