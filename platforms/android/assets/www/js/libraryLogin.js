/*=============================================
ACTUALIZAR DATA DE LOGIN
=============================================*/

function refresData(origen,data){

    if(origen == 'ws'){

        $("#"+data).val($("#"+data).val());
        //Guardado en localStorage los datos del Host o WebService
        window.localStorage.setItem("webservice",$("#"+data).val());
    }

    if(origen == 'bd'){

        $("#"+data).val($("#"+data).val());
        //Guardado en localStorage los datos de la base de datos.
        window.localStorage.setItem("basededatos",$("#"+data).val());
    }

    if(origen == 'user'){

        $("#"+data).val($("#"+data).val());
        $(".nameUser").val($("#"+data).val());

        //Guardado en localStorage los datos del usuario.
        window.localStorage.setItem("user",$("#"+data).val());
    }
    
    if(origen == 'pass'){

        $("#"+data).val($("#"+data).val());

        //Guardado en localStorage los datos de la contraseña.
        window.localStorage.setItem("pass",$("#"+data).val());

    } 

}

/*=============================================
LOGIN CON ITRIS
=============================================*/

function ItsLogin(){
    var d = new Date();

    var $btn = $("#ItsLogin").button('loading');

    var ItsLogin = new FormData();
    /*
    ItsLogin.append("ws", "http://iserver.itris.com.ar:6103/ITSWS/ItsCliSvrWS.asmx?WSDL" );
    ItsLogin.append("base", "MERCADOLIBRE" );
    ItsLogin.append("usuario", "ADMINISTRADOR" );
    ItsLogin.append("pass", "4583" );
    */

    var data = [$("#inputWS").val(), $("#inputBD").val(), $("#inputUser").val(), $("#inputPass").val()];
    var dataLabel = ['WebService','Base de datos','Usuario','Password'];

    for(var i=0; i < data.length; i++){

        if(data[i] == ''){

            navigator.notification.alert(
                '¡El campo ' + dataLabel[i] + ' no puede estar vacío!',
                function mensaje() {
                    //...
                },
                'VALIDACIÓN',
                'Listo'
            );            
            return;
        }

    }

    ItsLogin.append("ws", "http://"+$("#inputWS").val()+"/ITSWS/ItsCliSvrWS.asmx?WSDL" );
    ItsLogin.append("base", $("#inputBD").val() );
    ItsLogin.append("usuario", $("#inputUser").val() );
    ItsLogin.append("pass", $("#inputPass").val() );

    $.ajax({
        url:"http://apidevelopers.hol.es/tostana/itslogin.php",
        method: "POST",
        data: ItsLogin,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta){
            
            $btn.button('reset');

            var ItsLoginResult = JSON.parse(respuesta).ItsLoginResult;
            var ItsSession = JSON.parse(respuesta).session;

            if(ItsLoginResult == 0){

                navigator.notification.alert(
                    'ID session correcto ' + ItsSession,
                    alertItsLoginResult,
                    'Inicio de sesion',
                    'Listo'
                );
                
                function alertItsLoginResult() {
                    
                    $(".loginBtnClose").trigger( "click" );

                    window.localStorage.setItem("FecUltActL",d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() );
                    $(".FecUltActL").html( d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() );

                    $(".nameUser").html( $("#inputUser").val() );
                }                

            }else{

                navigator.notification.alert(
                    JSON.parse(respuesta).motivo,
                    function mesjee() {
                        
                    },
                    'ALERTA',
                    'Cerrar'
                );                                

            }
        
        }

    })  

}