/*=============================================
LOGIN CON ITRIS
=============================================*/

$("#ItsSaveGesRep").click(function(){

    var data = [$("#iddEmpresa").val(), $("#addEmpresaGestion").val(), $("#addMotivoG").val(), $("#addObservacionesG").val()];
    var dataLabel = ["ID","Nombre de la empresa","Motivo","Observaciones"];

    for(var i=0; i < data.length; i++){

        if(data[i]==""){

            navigator.notification.alert(
                "El campo " + dataLabel[i] + " no puede estar vacío.",
                function mmm() {
                    
                },
                'Validación',
                'Volver a intentar'
            );            
            
            return;

        }
        
    }

    pushGestion($("#iddEmpresa").val(),$("#addMotivoG").val(),$("#addObservacionesG").val(),'yes');

})

$(document).on('click', '.checkClient', function(e) {

    if(window.localStorage.getItem('comprobante') == 1){//remito
        //alert(36);
        $("#iddEmpresaRemito").val($(this).attr('idEmpresa'));
        
        //Marco la APP para para saber cual es la empresa seleccionada.
        window.localStorage.setItem("id_empresa", $(this).attr('idEmpresa') );
        window.localStorage.setItem("id_des_empresa", $(this).attr('desc') );
        
            $(".addArticles").show();
        $("#addEmpresaRemito").val($(this).attr('desc'));
    
        $("#closePopUpClient").trigger( "click" );

    }else if(window.localStorage.getItem('comprobante') == 2){//Factura

        alert('2');

    }else{
        //alert(48);
        $("#iddEmpresa").val($(this).attr('idEmpresa'));
            $(".addArticles").show();
        $("#addEmpresaGestion").val($(this).attr('desc'));
    
        $("#closePopUpClient").trigger( "click" );       

    }


});


/*=============================================
REFRESCAR LUEGO DE SELECCIONAR ARTICULO o CANCELAR LA BUSQUEDA
=============================================*/

$("#closePopUpClient, #closePopUpClientX").click(function(){

    if(window.localStorage.getItem('comprobante') == 1){//remito

        $("#addEmpresasC").trigger( "click" ); 
    
        setTimeout(function(){ $("#addEmpresasC").trigger( "click" ); }, 1000); 

    }else if(window.localStorage.getItem('comprobante') == 2){//factura


    }else{

        $("#addEmpresas").trigger( "click" ); 
    
        setTimeout(function(){ $("#addEmpresas").trigger( "click" ); }, 1000);        

    }

})

/*=============================================
INSERTAR GESTION EMPRESAS
=============================================*/
//id INTEGER PRIMARY KEY AUTOINCREMENT, FECHA, CLIENTE, MOTIVO, USUARIO, OBSERVACIONES
//[$("#iddEmpresa").val(), $("#addEmpresaGestion").val(), $("#addMotivoG").val(), $("#addObservacionesG").val()];
function pushGestion(id,motivo,observaciones,popup){

    var d = new Date();

    //Fecha Actual
    var dateNow = d.getFullYear() +'/'+ d.getMonth() +'/'+ d.getDate() +' '+ d.getMinutes() + ':' + d.getSeconds();

    //Usuario
    var usuario = $("#inputUser").val();

    //Razón social
    var raz_social = $("#addEmpresaGestion").val();

    db = window.openDatabase("TOSTANAPP", "1.0", "Tostanapp", 200000);

    db.transaction(function populateDBF(tx){

        tx.executeSql('INSERT INTO EST_CLI_APP (fecha, cliente, raz_social, motivo, usuario, observaciones, gestionado) VALUES ("'+dateNow+'", "'+id+'", "'+raz_social+'", "'+motivo+'", "'+usuario+'", "'+observaciones+'", "G" )');

        },function errorCBF(err){

            if(popup == 'yes'){

                navigator.notification.alert(
                    "Error procesando SQL (" + err.code + ') - ' + err.message,
                    function mesje() {
                        
                    },
                    'ALERTA',
                    'Cerrar'
                );                

            }else{
                
                return false;

            }

        },function successCBF(){
            
            $( ".loginBtnClose" ).trigger( "click" );

            $("#addEmpresaGestion").val('');
            $("#iddEmpresa").val('');
            
            $("#addMotivoG").val('');
            $("#addObservacionesG").val('');

            if(popup == 'yes'){

                navigator.notification.alert(
                    'Datos guardados con éxito',
                    function mensaje() {
                        window.localStorage.setItem("FecUltActG",d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() );
                        $(".FecUltActG").html( d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() );
                    },
                    'Alta de cliente',
                    'Listo'
                );

            }else{

                return true;

            }

        }
    );

}