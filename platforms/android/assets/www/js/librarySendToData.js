/*=============================================
GUARDAR REMITO
=============================================*/
$("#SendAllData").click(function(){

    //DESCARGA DE PEDIDOS
    db = window.openDatabase("TOSTANAPP", "1.0", "Tostanapp", 200000);

    db.transaction(function cargaRegistros(tx){

        tx.executeSql("select * from APP_PEDIDOS where centralizado isnull ", [], function cargaDatosSuccess(tx, results){

            if(results.rows.length == 0){

                navigator.notification.alert(
                    "No tenés [Comprobantes] guardados para descargar.",
                    function msje() {},
                    'ALERTA',
                    'Cerrar'
                );

            }else{
                
                for(var i=0; i<results.rows.length; i++){

                    var art = results.rows.item(i);

                    var orders = new FormData();
                    orders.append("idd_app", art.ID );
                    orders.append("date", art.FECHA );
                    orders.append("data", art.DATA );
                    orders.append("users", $("#inputUser").val() );

                    $.ajax({
                        url:"http://apidevelopers.hol.es/tostana/repo/orders.php",
                        method: "POST",
                        data: orders,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(respuesta){
                            var res = JSON.parse(respuesta);
                            //var res = JSON.stringify(respuesta);
                            //alert(res.Estado + ' - ' +res.Mensaje);
                            if(res.Estado=='1'){
                                alert('Existió  un error al descargar el pedido. ' + res.Mensaje);
                            }else{
                                deleteFromID('APP_PEDIDOS', res.id);
                            }
                        }
                    
                    });

                    //alert(art.ID + ' - ' + art.FECHA + ' - ' + art.DATA);                   

                } 
                
                navigator.notification.alert(
                    "Descargamos los [Comprobantes] correctamente",
                    function msje() {},
                    'MENSAJE',
                    'Cerrar'
                );                

            }

        }, function errorDB(err){

            navigator.notification.alert("Error procesando SQL (errorDB! Linea 635):" + err.message, alertDismissed, 'Pedidos Mobile', 'Listo');

        });

    }, function errorDB(err){

        navigator.notification.alert("Error procesando SQL (errorDB! Linea 635):" + err.message, alertDismissed, 'Pedidos Mobile', 'Listo');

    });

    //DESCARGA DE GESTIONADOS
    db.transaction(function cargaRegistros(tx){

        tx.executeSql("select * from EST_CLI_APP WHERE CENTRALIZADO isnull ", [], function cargaDatosSuccess(tx, results){

            if(results.rows.length == 0){

                navigator.notification.alert(
                    "No tenés [Gestiones de Reparto] para descargar",
                    function msje() {},
                    'ALERTA',
                    'Cerrar'
                );

            }else{
                
                for(var i=0; i<results.rows.length; i++){

                    var art = results.rows.item(i);

                    var gestionados = new FormData();
                    gestionados.append("id", art.id );
                    gestionados.append("fecha", art.FECHA );
                    gestionados.append("cliente", art.CLIENTE );
                    gestionados.append("raz_social", art.RAZ_SOCIAL );
                    gestionados.append("motivo", art.MOTIVO );
                    gestionados.append("usuario", art.USUARIO );
                    gestionados.append("observaciones", art.OBSERVACIONES );
                    gestionados.append("gestionados", art.GESTIONADO );


                    $.ajax({
                        url:"http://apidevelopers.hol.es/tostana/repo/gestion.php",
                        method: "POST",
                        data: gestionados,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(respuesta){
                            var res = JSON.parse(respuesta);
                            if(res.Estado=='1'){
                                alert('Existió  un error al descargar gestionado. ' + res.Mensaje);
                            }else{//Debo devolver desde la API el ID
                                deleteFromID('EST_CLI_APP', res.id);
                            }
                        }
                    
                    });                  

                } 
                
                navigator.notification.alert(
                    "Descargamos las [Gestiones de Reparto] correctamente",
                    function msje() {},
                    'MENSAJE',
                    'Cerrar'
                );                

            }

        }, function errorDB(err){

            navigator.notification.alert("Error procesando SQL (errorDB! Linea 635):" + err.message, alertDismissed, 'Pedidos Mobile', 'Listo');

        });

    }, function errorDB(err){

        navigator.notification.alert("Error procesando SQL (errorDB! Linea 635):" + err.message, alertDismissed, 'Pedidos Mobile', 'Listo');

    });

    //DESCARGA DE NUEVOS CLIENTES
    db.transaction(function cargaRegistros(tx){

        tx.executeSql("select * from ERP_CLIENTES_MAIL WHERE centralizado isnull ", [], function cargaDatosSuccess(tx, results){

            if(results.rows.length == 0){

                navigator.notification.alert(
                    "No tenés [Alta de cliente] para descargar",
                    function msje() {},
                    'ALERTA',
                    'Cerrar'
                );

            }else{
                
                for(var i=0; i<results.rows.length; i++){

                    var art = results.rows.item(i);

                    var altas = new FormData();
                    altas.append("id", art.id );
                    altas.append("raz_social", art.raz_social );
                    altas.append("tel", art.tel );
                    altas.append("tipDoc", art.tipDoc );
                    altas.append("catIva", art.catIva );
                    altas.append("nomDoc", art.numDoc );
                    altas.append("Detalle", art.Detalle );

                    $.ajax({
                        url:"http://apidevelopers.hol.es/tostana/repo/altas.php",
                        method: "POST",
                        data: altas,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(respuesta){
                            var res = JSON.parse(respuesta);
                            if(res.Estado=='1'){
                                alert('Existió  un error al descargar gestionado. ' + res.Mensaje);
                            }else{
                                deleteFromID('ERP_CLIENTES_MAIL', res.id);
                            }
                        }
                    
                    });
                    
                    var ItsSendMail = new FormData();
                    ItsSendMail.append("id", art.id );
                    ItsSendMail.append("AddRazSoc", art.raz_social );
                    ItsSendMail.append("addTel", art.tel );
                    ItsSendMail.append("addTipDoc", art.tipDoc );
                    ItsSendMail.append("addCatIva", art.catIva );
                    ItsSendMail.append("addNumDoc", art.numDoc );
                    ItsSendMail.append("addObser", art.Detalle );
                
                    $.ajax({
                        url:"http://apidevelopers.hol.es/tostana/sendMail.php",
                        method: "POST",
                        data: ItsSendMail,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(respuesta){
                            
                            $btn.button('reset');
                
                            var ItsSendMailResult = JSON.parse(respuesta).ItsSendMailResult;
                            var Msj = JSON.parse(respuesta).Msj;
                
                            if(ItsSendMailResult == "1"){
                
                                $("#AddRazSoc").val('');
                                $("#addTel").val('');
                                $("#addTipDoc").val('');
                                $("#addCatIva").val('');
                                $("#addNumDoc").val('');
                                $("#addObser").val('');              
                
                                navigator.notification.alert(
                                    Msj,
                                    alertItsSendMail,
                                    'Alta de cliente',
                                    'Listo'
                                );
                                
                                function alertItsSendMail() {

                                }                

                            }else{
                
                                navigator.notification.alert(
                                    JSON.parse(respuesta).Msj,
                                    function mesjes() {
                                        
                                    },
                                    'ALERTA',
                                    'Cerrar'
                                );                
                
                            }
                        
                        },
                        error : function(e){
                                            navigator.notification.alert(
                                                'Aparentemente el servidor está tardando un poco. Si el problema persiste notificar a lcondori@gmail.com',
                                                function mesje() {

                                                },
                                                'ALERTA',
                                                'Cerrar'
                                            );
                                            
                        },
                
                    timeout : 5000
                    }).done(function() {

                    }).fail(function() {
                        navigator.notification.alert(
                            'Existió un problema para enviar el mail, pero tranquilo, lo guardamos y lo vamos a volver a enviar cuando tengas conexión.',
                            function mesje() {
                                
                            },
                            'ALERTA',
                            'Cerrar'
                        );        
                
                    }).always(function() {

                    });

                } 
                
                navigator.notification.alert(
                    "Descargamos las [Alta de cliente] correctamente",
                    function msje() {},
                    'MENSAJE',
                    'Cerrar'
                );                

            }

        }, function errorDB(err){

            navigator.notification.alert("Error procesando SQL (errorDB! Linea 635):" + err.message, alertDismissed, 'Pedidos Mobile', 'Listo');

        });

    }, function errorDB(err){

        navigator.notification.alert("Error procesando SQL (errorDB! Linea 635):" + err.message, alertDismissed, 'Pedidos Mobile', 'Listo');

    });

    syncToItris();
})

//Marcar los pedidos como centralizado.
function deleteFromID(table, id){

    db = window.openDatabase("TOSTANAPP", "1.0", "Tostanapp", 200000);
    db.transaction(
        function populateDB(tx) {

            tx.executeSql('UPDATE ' + table + ' SET CENTRALIZADO = "1" WHERE ID = "'+id+'" ');

        },function errorCB(err) {

            navigator.notification.alert(
                "ATENCIÓN EXISTIÓ UN ERROR GRAVE EN CENTRALIZACION DE LA APP, POR FAVOR CONTACTESE CON (lcondori@gmail.com) " + err.message,
                function msje2() {

                },
                'INFORMACIÓN',
                'Cerrar'
            );                

        },function successCB() {              
            console.log(table + ' - ' + id);
        }
    );

}

//ENVIAR TODO A ITRIS.
function syncToItris(){

    var gestionados = new FormData();
    gestionados.append("available", "TRUE");

    $.ajax({
        url:"http://apidevelopers.hol.es/tostana/gestion.php",
        method: "GET",
        data: gestionados,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta){
            var res = JSON.parse(respuesta);

            navigator.notification.alert(
                "Se sincronizó a ITRIS " + res.Cantidad + " gestiones.",
                function msjess() {},
                'INFORMACIÓN',
                'Cerrar'
            );

        }

    });

    var comprobantes = new FormData();
    comprobantes.append("available", "TRUE");

    $.ajax({
        url:"http://apidevelopers.hol.es/tostana/procesarcomprobante.php",
        method: "GET",
        data: comprobantes,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta){
            var res = JSON.parse(respuesta);

            navigator.notification.alert(
                "Se sincronizó a ITRIS " + res.Cantidad + " comprobantes.",
                function mas() {},
                'INFORMACIÓN',
                'Cerrar'
            );

        }

    });     

}