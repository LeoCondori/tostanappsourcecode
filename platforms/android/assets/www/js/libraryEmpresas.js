/*=============================================
INSERTAR EMPRESA
=============================================*/

    $("#addEmpresas, #addEmpresasC").click(function(){

        if($(this).attr('id') == 'addEmpresas'){
            var query = 'SELECT ID_EMPRESA, DESCRIPCION FROM PRE_VEN_APP WHERE ID_EMPRESA not in (SELECT CLIENTE FROM EST_CLI_APP WHERE GESTIONADO = "G") GROUP BY ID_EMPRESA, DESCRIPCION';
        }else{
            var query = 'select ID_EMPRESA, DESCRIPCION from PRE_VEN_APP GROUP BY ID_EMPRESA, DESCRIPCION';
        }

        window.localStorage.setItem("query",query);

        db = window.openDatabase("TOSTANAPP", "1.0", "Tostanapp", 200000);

        db.transaction(function cargaRegistros(tx){

            var query = window.localStorage.getItem("query"); 

            tx.executeSql(query, [], function cargaDatosSuccess(tx, results){

                if(results.rows.length == 0){

                    navigator.notification.alert(
                        "No tenés información sobre empresas guardadas. Para solucionar este problema, probá iniciar sesión y luego presioná el botón SUBIR.",
                        function msje() {
                            //location.reload();
                        },
                        'ALERTA',
                        'Cerrar'
                    );

                }else{

                    $("#rowEmpresas").html('');
                    
                    $(".LabelSearchEmpresas").html('('+ results.rows.length +') Buscador de empresas');
                    
                    for(var i=0; i<results.rows.length; i++){

                        var art = results.rows.item(i);

                            $("#rowEmpresas").append('<tr><td scope="row" style="font-size:18px;">'+ art.ID_EMPRESA +'</td><td class="text-uppercase checkClient" idEmpresa="'+ art.ID_EMPRESA +'" desc="'+ art.DESCRIPCION +'" style="font-size:18px;">'+ art.DESCRIPCION +'</td></tr>');

                    }  

                }

            }, function errorDB(err){

                navigator.notification.alert("Error procesando SQL (errorDB! Linea 635):" + err.message, alertDismissed, 'Pedidos Mobile', 'Listo');

            });

        }, function errorDB(err){

            navigator.notification.alert("Error procesando SQL (errorDB! Linea 635):" + err.message, alertDismissed, 'Pedidos Mobile', 'Listo');

        });  
        
    })

/*=============================================
BUSCAR EMPRESA
=============================================*/

    // Escribir en el evento keyup del elemento de entrada de palabra clave
    $("#kwd_search_emp").keyup(function(){
        // Cuando el valor de la entrada no está en blanco
        if( $(this).val() != ""){
            // Mostrar solo TR correspondiente, ocultar el resto de ellos
            $("#table_search_emp tbody>tr").hide();
            //alert($(this).val().toUpperCase());
            $("#table_search_emp td:contains('" + $(this).val().toUpperCase() + "')").parent("tr").show();
        }else{
            // Cuando no hay entrada o limpia nuevamente, muestra todo de nuevo
            $("#table_search_emp tbody>tr").show();
        }
    });

/*=============================================
BUSCAR ARTICULOS
=============================================*/

    // Escribir en el evento keyup del elemento de entrada de palabra clave
    $("#kwd_search_art").keyup(function(){

        // Cuando el valor de la entrada no está en blanco
        if( $(this).val() != ""){
            // Mostrar solo TR correspondiente, ocultar el resto de ellos
            $("#table_search_art tbody>tr").hide();
            //alert($(this).val().toUpperCase());
            $("#table_search_art td:contains('" + $(this).val() + "')").parent("tr").show();
        }else{
            // Cuando no hay entrada o limpia nuevamente, muestra todo de nuevo
            $("#table_search_art tbody>tr").show();
        }
    });

/*=============================================
INSERTAR EMPRESA
=============================================*/

$("#saveCompany").click(function(){

    var data = [$("#AddRazSoc").val(), $("#addTel").val(), $("#addTipDoc").val(), $("#addCatIva").val(), $("#addNumDoc").val(), $("#addObser").val()];
    var dataLabel = ['Razón Social', 'Teléfono', 'Tipo de documento', 'Categoría de IVA', 'Número de documento','Detalle del pedido'];

    for(var i=0; i < data.length; i++){

        if(data[i] == ''){

            navigator.notification.alert(
                '¡El campo ' + dataLabel[i] + ' no puede estar vacío!',
                function mensaje() {

                },
                'VALIDACIÓN',
                'Listo'
            );            
            return;
        }

    }
    
    pushCompany($("#AddRazSoc").val(),$("#addTel").val(),$("#addTipDoc").val(),$("#addCatIva").val(),$("#addNumDoc").val(),$("#addObser").val(),'yes');

})


/*=============================================
INSERTAR EMPRESAS
=============================================*/

function pushCompany(razonSocial,tel,tipdoc,catIva,numDoc,detalle,popup){
    
    var d = new Date();

    db = window.openDatabase("TOSTANAPP", "1.0", "Tostanapp", 200000);

    db.transaction(function populateDBF(tx){

        tx.executeSql('INSERT INTO ERP_CLIENTES_MAIL (raz_social, tel, tipDoc, catIva, numDoc, Detalle) VALUES ("'+razonSocial+'", "'+tel+'", "'+tipdoc+'", "'+catIva+'", "'+numDoc+'", "'+detalle+'" )');

        },function errorCBF(err){

            if(popup == 'yes'){

                navigator.notification.alert(
                    "Error procesando SQL (" + err.code + ') - ' + err.message,
                    function mesje() {
                        
                    },
                    'ALERTA',
                    'Cerrar'
                );

            }else{
                
                return false;

            }

        },function successCBF(){
            
            $( ".btnClose" ).trigger( "click" );

            $("#AddRazSoc").val('');
            $("#addTel").val('');
            $("#addTipDoc").val('');
            $("#addCatIva").val('');
            $("#addNumDoc").val('');
            $("#addObser").val('');

            if(popup == 'yes'){

                navigator.notification.alert(
                    'Empresa guardada con éxito',
                    function mensaje() {
                        window.localStorage.setItem("FecUltActE",d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() );
                        $(".FecUltActE").html( d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() );
                    },
                    'Alta de cliente',
                    'Listo'
                );

            }else{

                return true;

            }

        }

    );

}


/*=============================================
ENVIAR MAIL
=============================================*/

function ItsSendMail(){
    
    var $btn = $(".btnSendMail").button('loading');
    var ItsSendMail = new FormData();
    ItsSendMail.append("AddRazSoc", $("#AddRazSoc").val() );
    ItsSendMail.append("addTel", $("#addTel").val() );
    ItsSendMail.append("addTipDoc", $("#addTipDoc").val() );
    ItsSendMail.append("addCatIva", $("#addCatIva").val() );
    ItsSendMail.append("addNumDoc", $("#addNumDoc").val() );
    ItsSendMail.append("addObser", $("#addObser").val() );

    $.ajax({
        url:"http://apidevelopers.hol.es/tostana/sendMail.php",
        method: "POST",
        data: ItsSendMail,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta){
            
            $btn.button('reset');

            var ItsSendMailResult = JSON.parse(respuesta).ItsSendMailResult;
            var Msj = JSON.parse(respuesta).Msj;

            if(ItsSendMailResult == "1"){

                $("#AddRazSoc").val('');
                $("#addTel").val('');
                $("#addTipDoc").val('');
                $("#addCatIva").val('');
                $("#addNumDoc").val('');
                $("#addObser").val('');              

                navigator.notification.alert(
                    Msj,  // message
                    alertItsSendMail, // callback
                    'Alta de cliente', // title
                    'Listo' // buttonName
                );
                
                function alertItsSendMail() {
                    // do something
                }                
                $( ".btnClose" ).trigger( "click" );
            }else{

                navigator.notification.alert(
                    JSON.parse(respuesta).Msj,
                    function mesjes() {
                        
                    },
                    'ALERTA',
                    'Cerrar'
                );                

            }
        
        },
        error : function(e){ 
                            $btn.button('reset'); 

                            navigator.notification.alert(
                                'Aparentemente el servidor está tardando un poco. Si el problema persiste notificar a lcondori@gmail.com',
                                function mesje() {
                                    
                                },
                                'ALERTA',
                                'Cerrar'
                            );
                            
        },

                            timeout : 5000
    }).done(function() {
        //navigator.notification.alert('Done solo cuando hay success');
    }).fail(function() {
        //Puede ser error de sintáxis del lado del servidor.
        //navigator.notification.alert('Existió un problema para enviar el mail, pero tranquilo, lo guardamos y lo vamos a volver a enviar cuando tengas conexión.');
        navigator.notification.alert(
            'Existió un problema para enviar el mail, pero tranquilo, lo guardamos y lo vamos a volver a enviar cuando tengas conexión.',
            function mesje() {
                
            },
            'ALERTA',
            'Cerrar'
        );        

    }).always(function() {
        //navigator.notification.alert('Always err succ');
        $btn.button('reset');

    });

}