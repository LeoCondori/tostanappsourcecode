/*=============================================
PROGRESS BAR
=============================================*/
function calculaPorcentaje(totalRegistros, proceso){

	var totalRegistros = parseInt(totalRegistros);
    var proceso = parseInt(proceso);
    var proceso = proceso + 1;
	var porcentaje = proceso * 100 / totalRegistros;

    $("#progressbars").html('<div class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="'+ parseInt(porcentaje) +'" aria-valuemin="0" aria-valuemax="'+ totalRegistros +'" style="width: '+ parseInt(porcentaje) +'%;">'+ parseInt(porcentaje) +'%</div></div>');
    
    if(porcentaje==100){
        navigator.notification.alert(
            'La sincronización finalizó con éxito',
            function mensaje() {
                location.reload();
            },
            'SINCRONIZACIÓN',
            'Listo'
        );
        /*setInterval(function(){ $(".loadingTxt").html(''); 
                                $("#progressbars").html(''); 
                            }, 3000);*/
    }
    
}

/*=============================================
LOGIN CON ITRIS
=============================================*/

$("#SyncPrice").click(function(){

    $(".loadingTxt").html('Conectando con Itris...');
    $("#progressbars").html('');

    var d = new Date();

    var $btn = $("#SyncPrice").button('loading');

    var ItsSyncPrice = new FormData();

    var data = [$("#inputWS").val(), $("#inputBD").val(), $("#inputUser").val(), $("#inputPass").val()];
    var dataLabel = ['WebService','Base de datos','Usuario','Password'];

    for(var i=0; i < data.length; i++){

        if(data[i] == ''){

            navigator.notification.alert(
                '¡El campo ' + dataLabel[i] + ' no puede estar vacío!',
                function mensaje() {
                    //...
                },
                'VALIDACIÓN',
                'Listo'
            );            
            return;
        }

    }

    ItsSyncPrice.append("ws", "http://"+$("#inputWS").val()+"/ITSWS/ItsCliSvrWS.asmx?WSDL" );
    ItsSyncPrice.append("base", $("#inputBD").val() );
    ItsSyncPrice.append("usuario", $("#inputUser").val() );
    ItsSyncPrice.append("pass", $("#inputPass").val() );

    $.ajax({
        url:"http://apidevelopers.hol.es/tostana/downloadPrice.php",
        method: "POST",
        data: ItsSyncPrice,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta){
            $btn.button('reset');

            window.localStorage.setItem("FecUltActM",d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() );
            $(".FecUltActM").html( d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds() );
            
            var ItsGetDataResult = JSON.parse(respuesta).ItsGetDataResult;
            var motivo = JSON.parse(respuesta).motivo;

            var Cantidad = JSON.parse(respuesta).Cantidad;
            var Data = JSON.parse(respuesta).Data;

            
            if(ItsGetDataResult== "1"){
                
                navigator.notification.alert(
                    motivo,
                    function mensajeMotivo() {
                        location.reload();
                    },
                    'VALIDACIÓN',
                    'Listo'
                );

            }

            console.log('CANTIDAD DE EMPRESAS' + Data.length);
            $(".loadingTxt").html('Actualizado artículos y precios de ventas...');
            for(x=0; x<Data.length; x++){
            
                pushPrice(Data[x]["ID"],Data[x]["DESCRIPCION"].toUpperCase(),Data[x]["ID_EMPRESA"],Data[x]["ID_VENDEDOR"],Data[x]["VENDEDOR"],Data[x]["PRE"],Data[x]["LISTA"],Data[x]["FK_ERP_ARTICULOS"],Data[x]["DES_ART"],Data[x]["PRECIO"],Data[x]["_FK_ITRIS_USERS"],'yes',Data.length,x);

            }
             
        },
        error : function(e){ 
                            $btn.button('reset');

                            navigator.notification.alert(
                                'Aparentemente el servidor está tardando un poco.',
                                function mesjes() {

                                },
                                'ALERTA',
                                'Cerrar'
                            );
                            
                        },
                            timeout : 20000
    }).done(function() {
        
        navigator.notification.alert('Done solo cuando hay success' +ItsLoginResult);
      }).fail(function() {
        //Puede ser error de sintáxis del lado del servidor.
        $(".loadingTxt").html('');
        navigator.notification.alert('Si el problema persiste enviar mail a lcondori@gmail.com');

      }).always(function() {
        
        //navigator.notification.alert('Always err succ');
        $btn.button('reset');
      });

})


/*=============================================
INSERTAR PRECIOS
=============================================*/

function pushPrice(id,descripcion,idEmpresas,idVendedor,vendedor,pre,lista,articulos,desArt,precio,user,popup,cantidad,registro){
    
    var d = new Date();

    db = window.openDatabase("TOSTANAPP", "1.0", "Tostanapp", 200000);

    db.transaction(function populateDBF(tx){

        tx.executeSql('DELETE FROM PRE_VEN_APP');

        },function errorCBF(err){
            navigator.notification.alert("Error procesando SQL (" + err.code + ') - ' + err.message);
        },function successCBF(){
            //console.log('Se borró la clase PRE_VEN_APP');
        }

    );

    db.transaction(function populateDBF(tx){

        tx.executeSql('INSERT INTO PRE_VEN_APP (ID, DESCRIPCION, ID_EMPRESA, ID_VENDEDOR, VENDEDOR, PRE, LISTA, FK_ERP_ARTICULOS, DES_ART, PRECIO, _FK_ITRIS_USERS) VALUES ("'+id+'", "'+descripcion+'", "'+idEmpresas+'", "'+idVendedor+'", "'+vendedor+'", "'+pre+'", "'+lista+'", "'+articulos+'", "'+desArt+'", "'+precio+'", "'+user+'" )');

        },function errorCBF(err){

            if(popup == 'yes'){

                navigator.notification.alert("Error procesando SQL (" + err.code + ') - ' + err.message);

            }else{
                
                return false;

            }

        },function successCBF(){

            if(popup == 'yes'){

                calculaPorcentaje(cantidad, registro);

            }else{

                return true;

            }

        }

    );

}