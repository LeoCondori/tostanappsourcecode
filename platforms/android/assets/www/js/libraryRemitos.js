//MARCA LA APP PARA REMITOS O FACTURA

function clickNow(tipo){

    if(tipo == 'remito'){

        window.localStorage.setItem('comprobante',1);

    }else if(tipo == 'factura'){

        window.localStorage.setItem('comprobante',2);

    }else{

        window.localStorage.setItem('comprobante',0);

    }

}

/*=============================================
GUARDAR REMITO
=============================================*/

$("#ItsSaveRemito").click(function(){

    //Valida la cabecera
    data = [$("#addEmpresaRemito").val(), $("#iddEmpresaRemito").val(), $("#addTipComp").val(), $("#addNumero").val()];
    dataLabel = ["Empresa","ID Empresa","Tipo de comprobante","Número de comprobante"];

    for(var i=0; i<data.length; i++){
        
        if(data[i]==""){
 
            navigator.notification.alert(
                "El campo " + dataLabel[i] + " no puede estar vacío.",
                function msje12() {

                },
                'VALIDACIÓN',
                'Aceptar'
            );

            return;
        }

    }

    if($("#addTipComp").val() == 'fve'){
        if($("#addCondition").val() == ''){
            navigator.notification.alert(
                "El campo [Condición] no puede estar vacío.",
                function msje123() {

                },
                'VALIDACIÓN',
                'Aceptar'
            );

            return;            
        }   
    }

    //Validar el detalle
    var subTotalQuanty=0;
    $(".quanty").each(function(){
        subTotalQuanty+=parseInt($(this).html()) || 0;
    });   

    if(subTotalQuanty==0){

        navigator.notification.alert(
            "No podemos guardar un comprobante sin ítems",
            function msje132() {

            },
            'VALIDACIÓN',
            'Aceptar'
        );

        return;
    }

    //ARMO EL ARRAY PARA GENERAR COMPROBANTES
        var comp = [];
        comp.push({empresa:$("#iddEmpresaRemito").val(), tipo:$("#addTipComp").val(), numero:$("#addNumero").val(), condicion:$("#addCondition").val(), observaciones:$("#addObsRemito").val(), articles: [], quanty:[]});

        //ARTICULO 
        $(".price").each(function(){
            comp[0].articles.push($(this).attr('producID'));
        });

        //CANTIDAD
        $(".quanty").each(function(){
            comp[0].quanty.push(parseInt($(this).html()));
        });

        var jsonRows = JSON.stringify(comp);

        pushComprobante(jsonRows,'yes');
    //ARMO EL ARRAY PARA GENERAR COMPROBANTES

        //Limpio las cabeceras.
        $("#addEmpresaRemito").val(''); 
        $("#iddEmpresaRemito").val(''); 
        $("#addTipComp").val('');

        //Limpio el detalle.
        $(".addArtForDet").html('');
        $(".rowList").html('');
        $(".priceCalculator").html('');
        $(".quantyCalculator").html('');    

        //Limpio el Footer
        $(".footerSubTotal").html('');
        $(".footerTotal").html('');

    /*
    navigator.notification.alert(
        "Comprobante generado con éxito",
        function closeComprobantejj() {

            //Limpio el buscado de artículos.
            getRowsArticles();

            //Cierro el comprobante.
            $(".loginBtnCloseRem").trigger( "click" ); 

        },
        'COMPROBANTE',
        'Aceptar'
    );*/

})

/*=============================================
AUMENTAR O DISMINUIR CANTIDAD
=============================================*/
$(document).on('click', '.menosArt', function(e) {//addTipComp
    
    //Obtendo el ID del elemento que estoy modificando.
    $(this).attr('identy');

    //Obtengo el número de cantidad existente.
    $("#cantItemEdit"+$(this).attr('identy')).val();

    var qty = parseInt($("#cantItemEdit"+$(this).attr('identy')).val());
    
    var newQty = qty - 1;
    
    if(newQty < 2){newQty = 1;}

    $("#cantItemEdit"+$(this).attr('identy')).val(newQty);

    //TENGO QUE REFRESCAR LOS SUBTOTALES.
    var priceUnit = parseInt($("#precioItemEdit"+$(this).attr('identy')).val());

    var newPrice = priceUnit * newQty;

    $("#precioTotItemEdit"+$(this).attr('identy')).val(newPrice);

    //AHORA REFRESCO LA INFORMACIÓN DE LA LISTA
    $("#precioTotItemList"+$(this).attr('identy')).html(newPrice);
    $("#cantItemList"+$(this).attr('identy')).html(newQty);

    //REFRESCO EL SUBTOTAL
    refresSubTotal();    
})

$(document).on('click', '.masArt', function(e) {

    //Obtendo el ID del elemento que estoy modificando.
    $(this).attr('identy');

    //Obtengo el número de cantidad existente.
    $("#cantItemEdit"+$(this).attr('identy')).val();

    var qty = parseInt($("#cantItemEdit"+$(this).attr('identy')).val());
    
    var newQty = qty + 1;

    $("#cantItemEdit"+$(this).attr('identy')).val(newQty);
        
    //TENGO QUE REFRESCAR LOS SUBTOTALES.
    var priceUnit = parseInt($("#precioItemEdit"+$(this).attr('identy')).val());

    var newPrice = priceUnit * newQty;

    $("#precioTotItemEdit"+$(this).attr('identy')).val(newPrice);

    //AHORA REFRESCO LA INFORMACIÓN DE LA LISTA
    $("#precioTotItemList"+$(this).attr('identy')).html(newPrice);
    $("#cantItemList"+$(this).attr('identy')).html(newQty);    

    //REFRESCO EL SUBTOTAL
    refresSubTotal();
})

/*=============================================
CAMBIAR DE VISTA
=============================================*/

$(".changeViewList").click(function(){

    $("#viewerEdit").removeClass( "btn btn-primary" ).addClass( "btn btn-default" );
    $(".addArtForDet").hide();

    $(".rowList").show();
    $("#viewerList").removeClass( "btn btn-default" ).addClass( "btn btn-primary" );

})

$(".changeViewEdit").click(function(){

    $("#viewerList").removeClass( "btn btn-primary" ).addClass( "btn btn-default" );
    $(".rowList").hide();

    $("#viewerEdit").removeClass( "btn btn-default" ).addClass( "btn btn-primary" );
    $(".addArtForDet").show();

})

/*=============================================
BUSCADOR DE ARTICULOS
=============================================*/
//addEmpresasC
//addArticles WHERE ID_EMPRESA = ' + id_empresa + ' 
//$("#addEmpresasC").click(function(){
function getRowsArticles(){

    db = window.openDatabase("TOSTANAPP", "1.0", "Tostanapp", 200000);

    db.transaction(function cargaRegistros(tx){
        //var id_empresa = $("#iddEmpresaRemito").val();

        var id_empresa = window.localStorage.getItem("id_empresa");
        
        //alert(id_empresa);
        
        tx.executeSql('select ID, FK_ERP_ARTICULOS, DES_ART, PRECIO, ID_EMPRESA from PRE_VEN_APP WHERE ID_EMPRESA = "' + id_empresa + '" ', [], function cargaDatosSuccess(tx, results){

            if(results.rows.length == 0){

                //alert("No tenés artículos aún en la aplicación, probá presionando el botón SUBIR.");

                navigator.notification.alert(
                    "No tenés artículos aún en la aplicación, probá iniciar sesión y luego presioná el botón SUBIR.",
                    function mesje() {
                        
                    },
                    'ALERTA',
                    'Cerrar'
                );

            }else{

                $("#rowArticulos").html('');
                
                $(".LabelbuscaArticulos").html('('+ results.rows.length +') Buscador de artículos');
                
                for(var i=0; i<results.rows.length; i++){

                    var art = results.rows.item(i);

                        $("#rowArticulos").append('<tr class="rowsAdd'+ art.ID +' toArtAdd" idd="'+art.ID+'" nombre="'+art.DES_ART+'" precio="'+art.PRECIO+'" articulo="'+art.FK_ERP_ARTICULOS+'"> '+
                                                        '<td scope="row" style="font-size:12px;">'+ art.FK_ERP_ARTICULOS +'</td> ' +
                                                        '<td idEmpresa="'+ art.DES_ART + ' " desc="'+ art.DES_ART +'" style="font-size:16px;">'+ art.DES_ART + ' </td> ' +
                                                        '<td idEmpresa="'+ art.PRECIO + ' " desc="'+ art.PRECIO +'" style="font-size:14px;">$ '+ art.PRECIO +'</td> ' +
                                                  '</tr>');

                }  

            }

        }, function errorDB(err){

            navigator.notification.alert("Error procesando SQL (errorDB! Linea 635):" + err.message, alertDismissed, 'Pedidos Mobile', 'Listo');

        });

    }, function errorDB(err){

        navigator.notification.alert("Error procesando SQL (errorDB! Linea 635):" + err.message, alertDismissed, 'Pedidos Mobile', 'Listo');

    });  
    
//})
}

/*=============================================
REFRESCAR LUEGO DE SELECCIONAR ARTICULO
=============================================*/

$("#closePopUpSearchArt, #closePopUpSearchArtX").click(function(){

    $("#addEmpresasC").trigger( "click" ); 
    
    setTimeout(function(){ $("#addEmpresasC").trigger( "click" ); }, 1000);

})


$(document).on('click', '.toArtAdd', function(e) {

    $(".addArtForDet").prepend('<li class="list-group-item itemPropEdit'+$(this).attr('idd')+'"> ' +
                                '<form class="form-inline"> ' +
                                    '<div class="form-group"> ' +
                                        '<label for="detalleArticulo"> Descripción</label> ' +
                                        '<input type="text" class="form-control input-lg" id="detalleArticulo" value="'+$(this).attr('nombre')+'" readonly> ' +
                                    '</div> ' +
                                    '<div class="form-group"> ' +
                                        '<div class="input-group"> ' +
                                            '<div class="input-group-addon">$</div> ' +
                                            '<input type="number" class="form-control input-lg" id="precioItemEdit'+$(this).attr('idd')+'" value="'+$(this).attr('precio')+'" readonly> ' +
                                            '<div class="input-group-addon">.00</div> ' +
                                        '</div> ' +
                                    '</div> ' +
                                    '<div class="form-group"> ' +
                                        '<div class="input-group col-xs-8"> ' +
                                            '<div class="input-group-addon btn-lg"><span class="glyphicon glyphicon-minus menosArt" identy="'+$(this).attr('idd')+'" aria-hidden="true"></span></div> ' +
                                            '<input type="number" class="form-control input-lg" id="cantItemEdit'+$(this).attr('idd')+'" value="1" readonly> ' +
                                            '<div class="input-group-addon btn-lg"><span class="glyphicon glyphicon-plus masArt" identy="'+$(this).attr('idd')+'" valor="mas'+$(this).attr('idd')+'" aria-hidden="true"></span></div> ' +
                                        '</div> ' +
                                    '</div> ' +

                                    '<div class="form-group"> ' +
                                        '<div class="input-group"> ' +
                                            '<div class="input-group-addon">$</div> ' +
                                            '<input type="number" class="form-control input-lg" id="precioTotItemEdit'+$(this).attr('idd')+'" value="'+$(this).attr('precio')+'" readonly> ' +
                                            '<div class="input-group-addon">.00</div> ' +
                                        '</div> ' +
                                    '</div> ' +                                    

                                    '<button type="button" class="btn btn-danger btn-lg quitarItem" iddForRemove="'+$(this).attr('idd')+'"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> Quitar</button> ' +
                                '</form> ' +
                            '</li>');

    $(".rowList").prepend('<li class="list-group-item iip itemPropList'+$(this).attr('idd')+'"> ' +
                            '<span class="badge priceTot" id="precioTotItemList'+$(this).attr('idd')+'">'+$(this).attr('precio')+'</span> ' +
                            '<span class="badge price" producID="' + $(this).attr('articulo') + '" id="precioItemList'+$(this).attr('idd')+'">'+$(this).attr('precio')+'</span> ' +
                            '<span class="badge quanty" id="cantItemList'+$(this).attr('idd')+'">1</span> ' +
                            '[' + $(this).attr('articulo') + '] '+$(this).attr('nombre')+' ' +
                         '</li> ');

    if ($(this).hasClass('success')){
        //Si ya está seleccionado entonces significa que lo voy a desseleccionar
        unSelect($(this).attr('idd'));
    }else{
        //Marco como seleccionado el producto
        $(".rowsAdd"+$(this).attr('idd')).addClass( "success" );
    }

    //Recalculo los subtotales.
    refresSubTotal();
})
    
//Cargar la tabla de artículos para buscador.
getRowsArticles();

//Cuando quiero agregar artículos desde la lupa, entonces filtro por la empresa seleccionada.
$(".addArticles").click(function(){

    getRowsArticles();

})
/*=============================================
QUITAR ITEM
=============================================*/    

$(document).on('click', '.quitarItem', function(e) {
    //Saco la selección del buscador.
    $(".rowsAdd"+$(this).attr('iddForRemove')).removeClass( "success" );
    
    //sacó de la vista el ítem. Editar.
    $( ".itemPropEdit"+$(this).attr('iddForRemove') ).remove();

    //sacó de la vista el ítem. Lista.
    $( ".itemPropList"+$(this).attr('iddForRemove') ).remove();

    //Falta recalcular los montos.
    refresSubTotal();
})


 //Esta función es llamada cuando el usuario vuelve
 //a seleccionar el artículo que ya seleccionó. Entonces
 //se asume que lo quiere quitar, se quita de todos lados.

function unSelect(idd){

    //Saco la selección del buscador.
    $(".rowsAdd"+idd).removeClass( "success" );
    
    //sacó de la vista el ítem. Editar.
    $( ".itemPropEdit"+idd).remove();

    //sacó de la vista el ítem. Lista.
    $( ".itemPropList"+idd).remove();

}

/*=============================================
CONSTRUYO EL SUBTOTAL
=============================================*/ 

function refresSubTotal(){

    var subTotalPrice=0;
    $(".priceTot").each(function(){
        subTotalPrice+=parseInt($(this).html()) || 0;
    });
    $(".priceCalculator").html('$ '+subTotalPrice);

    var subTotalQuanty=0;
    $(".quanty").each(function(){
        subTotalQuanty+=parseInt($(this).html()) || 0;
    });    
    $(".quantyCalculator").html(subTotalQuanty);

    //ACTUALIZO LOS TOTALES DEL FOOTER 
    $(".footerSubTotal").html('$ '+subTotalPrice);
    $(".footerTotal").html('$ '+subTotalPrice);
}

/*=============================================
INSERTAR EMPRESAS
=============================================*/

function pushComprobante(data,popup){

    var f = new Date();

    var FullDate = f.getFullYear() + '/' + f.getMonth() + '/' + f.getDate();

    db = window.openDatabase("TOSTANAPP", "1.0", "Tostanapp", 200000);

    db.transaction(function populateDBF(tx){

        tx.executeSql("INSERT INTO APP_PEDIDOS (fecha, data) VALUES ('"+FullDate+"', '"+data+"' )");

        },function errorCBF(err){

            if(popup == 'yes'){

                navigator.notification.alert(
                    "Error procesando SQL (" + err.code + ') - ' + err.message + '. Intentando insertar este query ' + data,
                    function mesje() {
                        
                    },
                    'ALERTA',
                    'Cerrar'
                );

            }else{
                
                return false;

            }

        },function successCBF(){
            
            $( ".btnClose" ).trigger( "click" );

            $("#AddRazSoc").val('');
            $("#addTel").val('');
            $("#addTipDoc").val('');
            $("#addCatIva").val('');
            $("#addNumDoc").val('');
            $("#addObser").val('');
            $("#addNumero").val('');
            $("#addCondition").val('');
            $("#addObsRemito").val('');

            if(popup == 'yes'){

                navigator.notification.alert(
                    'Comprobante guardado con éxito',
                    function mensaje() {
                        $(".addArticles").hide();
                    },
                    'COMPROBANTE',
                    'Listo'
                );

            }else{

                return true;

            }

        }
    );

    //Esto hace que la transacción se guarde en gestionados, entonces evita tener
    //que ingresar un motivo en gestionado.

    db.transaction(function populateDBF(tx){
        var f = new Date();
        var FullDate = f.getFullYear() + '/' + f.getMonth() + '/' + f.getDate();
        var id_empresa = window.localStorage.getItem("id_empresa");
        var id_des_empresa = window.localStorage.getItem("id_des_empresa");
        var motivo = 'C';
        var usuario = $("#inputUser").val();
        var observaciones = 'Realizó un pedido';
        
        tx.executeSql('INSERT INTO EST_CLI_APP (fecha, cliente, raz_social, motivo, usuario, observaciones, gestionado) VALUES ("'+FullDate+'", "'+id_empresa+'", "'+id_des_empresa+'", "'+motivo+'", "'+usuario+'", "'+observaciones+'", "G" )');

        },function errorCBF(err){

            if(popup == 'yes'){

                navigator.notification.alert(
                    "Error procesando SQL (" + err.code + ') - ' + err.message,
                    function mesje() {

                    },
                    'ALERTA',
                    'Cerrar'
                );                

            }else{
                
                return false;

            }

        },function successCBF(){

            if(popup == 'yes'){

                navigator.notification.alert(
                    'Cliente marcado en gestionados',
                    function mensajeevs() {

                    },
                    'Alta en gestion',
                    'Listo'
                );

            }else{

                return true;

            }

        }
    );

}

/*=============================================
PRUEBA DE GUARDADO
=============================================*/

$("#ItsTestRemito").click(function(){

    var comp = [];
    comp.push({empresa:$("#iddEmpresaRemito").val(), tipo:$("#addTipComp").val(), articles: [], quanty:[]});

    //ARTICULO 
    $(".price").each(function(){
        comp[0].articles.push($(this).attr('producID'));
    });

    //CANTIDAD
    $(".quanty").each(function(){
        comp[0].quanty.push(parseInt($(this).html()));
    });

    var jsonRows = JSON.stringify(comp);

})

/*=============================================
CONTROLA CAMBIO EN TIPO DE COMPROBANTE
=============================================*/
$(document).on('change', '#addTipComp', function(e) {

    if($("#addEmpresaRemito").val()==''){

        function alertDismissed() {
            // do something
        }

        navigator.notification.alert(
            'Tenés que elegir una empresa',  // message
            alertDismissed,         // callback
            'Atención',            // title
            'Ok'                  // buttonName
        );
        $("#addTipComp").val('');
        return;
    }
    
    //Si cambia el tipo de comprobante limpio el número de documento.
    $("#addNumero").val('');

    //Si es remito, pido el número.
    //Si es factura, pido el número y muestro la condición. 
    var tipo = $(this).val();

    function onPrompt(results) {
        //alert("You selected button number " + results.buttonIndex + " and entered " + results.input1);

        if (!/^([0-9])*$/.test(results.input1)){
            
            function alertDismissed() {
                // do something
            }
            
            navigator.notification.alert(
                'El valor ' + results.input1 + ' no es un número',  // message
                alertDismissed,         // callback
                'Atención',            // title
                'Ok'                  // buttonName
            );

            //alert("El valor " + results.input1 + " no es un número");
            return;
        }
        
        $("#addNumero").val(results.input1);

        if($("#addNumero").val().length > 8){

            navigator.notification.alert(
                'El valor ingresado [' + $("#addNumero").val() + '] supera los 8 dígitos.',
                alertDismissed,
                'Atención',
                'Ok'
            );

            $("#addNumero").val('');
            
            return;
        }
        if(tipo == 'fve'){
            var f = new Date();

            var year = f.getFullYear();
            var month = f.getMonth();
            var date = f.getDate();
            var codEmp = $("#iddEmpresaRemito").val();
            //alert(year+''+month+''+date+'-'+codEmp+'-'+zfill($("#addNumero").val(),8) );
            $("#addNumero").val(year+''+month+''+date+'-'+codEmp+'-'+zfill($("#addNumero").val(),8) );        

            //$("#addNumero").val(results.input1);
        }else{
            $("#addNumero").val(zfill(results.input1,8)); 
        }
    }
    
    navigator.notification.prompt(
        'Por favor ingresá el número de comprobante',  // message
        onPrompt,                  // callback to invoke
        'Registration',            // title
        ['Ok','Exit'],             // buttonLabels
        $("#addNumero").val()      // defaultText
    );

    if(tipo == 'rem'){

        $("#addCondition").val('');
        $("#condicion").hide();
    }

    if(tipo == 'fve'){

        $("#condicion").show();
    }

})

/*=============================================
CONTROLA CAMBIO EN NÚMERO DE COMPROBANTE
=============================================*/

$(document).on('click', '#addNumero', function(e) {

    if( $("#addEmpresaRemito").val()=='' ){

        function alertDismissed() {
            // do something
        }

        navigator.notification.alert(
            'Tenés que elegir una empresa',  // message
            alertDismissed,         // callback
            'Atención',            // title
            'Ok'                  // buttonName
        );
        $("#addTipComp").val('');
        return;
    }    

    function onPrompt(results) {
        //alert("You selected button number " + results.buttonIndex + " and entered " + results.input1);

        if (!/^([0-9])*$/.test(results.input1)){


            function alertDismissed() {
                // do something
            }
            
            navigator.notification.alert(
                'El valor ' + results.input1 + ' no es un número',  // message
                alertDismissed,         // callback
                'Atención',            // title
                'Ok'                  // buttonName
            );

            //alert("El valor " + results.input1 + " no es un número");
            return;
        }

        $("#addNumero").val(results.input1);

        if($("#addNumero").val().length > 8){

            navigator.notification.alert(
                'El valor ingresado [' + $("#addNumero").val() + '] supera los 8 dígitos.',
                alertDismissed,
                'Atención',
                'Ok'
            );

            $("#addNumero").val('');
            
            return;
        }

        if(tipo == 'fve'){
            var f = new Date();

            var year = f.getFullYear();
            var month = f.getMonth();
            var date = f.getDate();
            var codEmp = $("#iddEmpresaRemito").val();
            //alert(year+''+month+''+date+'-'+codEmp+'-'+zfill($("#addNumero").val(),8) );
            $("#addNumero").val(year+''+month+''+date+'-'+codEmp+'-'+zfill($("#addNumero").val(),8) );        

        }else{

            $("#addNumero").val(zfill(results.input1,8));

        }

        //$("#addNumero").val(results.input1);
    }

    navigator.notification.prompt(
        'Por favor ingresá el número de comprobante',  // message
        onPrompt,                  // callback to invoke
        'Registration',            // title
        ['Ok','Exit'],             // buttonLabels
        $("#addNumero").val()      // defaultText
    );

})

function zfill(number, width) {
    var numberOutput = Math.abs(number); /* Valor absoluto del número */
    var length = number.toString().length; /* Largo del número */ 
    var zero = "0"; /* String de cero */  
    
    if (width <= length) {
        if (number < 0) {
             return ("-" + numberOutput.toString()); 
        } else {
             return numberOutput.toString(); 
        }
    } else {
        if (number < 0) {
            return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
        } else {
            return ((zero.repeat(width - length)) + numberOutput.toString()); 
        }
    }
}